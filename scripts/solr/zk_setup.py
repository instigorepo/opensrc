import subprocess
import os
import crypt
import sys
import traceback
import re
import glob
import shutil
from tempfile import mkstemp
from optparse import OptionParser
#from urllib2 import urlopen,URLError, HTTPError
from random import randint

from install.components import InstallServerComponents
from cloud.zookeeper import ZooKeeper
from zoocfg import zoocfg
from start import start
from stop import stop
from status import status

servers = []
HOME_PATH = os.getenv("HOME")
usage = "usage: %prog [options] zookeeper_dir output_dir"
parser = OptionParser(usage=usage)
parser.add_option("-c","--count", dest="count", default=3, help="ensemble size (default is 3)")
parser.add_option("","--servers", dest="servers",
    default="localhost", help="explicit list of comma separated server names (alternative to --count)")
parser.add_option("","--clientportstart", dest="clientportstart", type='int',
    default=2181, help="first client port (default 2181)")
parser.add_option("", "--quorumportstart", dest="quorumportstart", type='int',
    default=3181, help="first quorum port (default 3181)")
parser.add_option("","--firstelectionportstart", dest="electionportstart", type='int',
    default=4181, help="first election port (default 4181)")
parser.add_option("","--homedir", dest="homedir", default="/root", help="path to home directory (i.e. /home/user)")
#parser.add_option("", "--maxClientConns", dest="maxClientConns", type='int',
#    default=10, help="maxClientConns of server config (default unspecified, ZK default)")
parser.add_option("","--electionAlg", dest="electionalg", type='int',
    default=3, help="electionAlg of server config (default unspecified, ZK default - FLE)")
(options, args) = parser.parse_args()
options.clientports = []
options.quorumports = []
options.electionports = []

if options.servers != "localhost":
    options.servers = options.servers.split(",")
    for i in xrange(1, len(options.servers) + 1) :
        options.clientports.append(options.clientportstart);
        options.quorumports.append(options.quorumportstart);
        options.electionports.append(options.electionportstart);
else :
    options.servers = []
    for i in xrange(options.count) :
        options.servers.append('localhost');
        options.clientports.append(options.clientportstart + i);
        options.quorumports.append(options.quorumportstart + i);
        options.electionports.append(options.electionportstart + i);

if (len(args) != 2):
    parser.error("Need zookeeper_dir in order to get jars/conf, and output_dir for where to put generated output")


def touch(filename, times=None):
    with file(filename, 'wa'):
        os.utime(filename, times)

def checkRootUser():
    if not os.geteuid() == 0:
        sys.exit("This script must be run as root or using the sudo command")

def findUser(name):
    try:
        proc = subprocess.Popen(['id', name], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = proc.communicate()

        if re.search(r"^id\:.*", err.rstrip("\r\n")):
            return False
        else:
            return True
    except:
        return None

def writefile(p, content):
    f = open(p, 'w')
    f.write(content)
    f.close()

def writescript(name, content):
    p = os.path.join(args[1], name)
    writefile(p, content)
    os.chmod(p, 755)

def copyjar(optional, srcs, jar, dstpath, dest):
    for src in srcs:
        try:
            shutil.copyfile(glob.glob(os.path.join(os.path.joing(*src), jar))[0],
                    os.path.join(dstpath,dst))
            return
        except:
            pass

    if optional: return

    print("Unable to find %s in %s" % (dst, args[0]))
    exit(1)

def addSolrUser(newPassword):
    encPass = crypt.crypt(newPassword,"22")
    os.system("useradd -p " + encPass + " solrdev ")    

def grep(patt, file):

    """ finds pattern in file - patt is a compiled regex"""
    matchlines = []
    for line in file:
        match = patt.search(line)
        if match:
            matchline = match.group()
            matchlines.append(matchline)
    results = '\n '.join(matchlines)
    if results:
        return results
    else:
        return None

if __name__ == '__main__':
    checkRootUser()
    zookeeper = ZooKeeper()
    zookeeper.Install()
    os.mkdir(args[1])
    serverlist = []
    for sid in xrange(1, len(options.servers) + 1):
        serverlist.append([sid,
            options.servers[sid - 1],
            options.clientports[sid - 1],
            options.quorumports[sid - 1],
            options.electionports[sid - 1]])

    for sid in xrange(1, len(options.servers) + 1) :
        serverdir = os.path.join(args[1], options.servers[sid - 1] +
            ":" + str(options.clientports[sid - 1]))

        os.mkdir(serverdir)
        os.mkdir(os.path.join(serverdir, "data"))
        conf = zoocfg(searchList = [{'sid' : sid,
                                        'servername' : options.servers[sid - 1],
                                        'clientPort' : options.clientports[sid - 1],
                                        'serverlist' : serverlist,
                                        #'maxClientConns' : options.#maxClientConns,
                                        'electionAlg' : options.electionalg}])
        writefile(os.path.join(serverdir,"zoo.cfg"), str(conf))
        writefile(os.path.join(serverdir,"data","myid"), str(sid))

    writescript("start.sh", str(start(searchList=[{'serverlist' : serverlist}])))
    writescript("stop.sh", str(stop(searchList=[{'serverlist' : serverlist}])))
    writescript("status.sh", str(status(searchList=[{'serverlist' : serverlist}])))
    
    content = """#!/bin/bash
    java -cp tmp/$1/*:. org.apache.zookeeper.ZooKeeperMain -server "$1"\n"""
    writescript("cli.sh", content)

    #if( os.path.isdir(args[1])):
    #    subprocess.call(['rm','-rf',args[1]])

    for f in glob.glob(os.path.join(args[0], 'lib', '*.jar')):
        shutil.copy(f, args[1]) 

    for f in glob.glob(os.path.join(args[0], 'src', 'java', 'lib', '*.jar')):
        shutil.copy(f, args[1])

    for f in glob.glob(os.path.join(args[0], 'build', 'lib', '*.jar')):
        shutil.copy(f, args[1])

    for f in glob.glob(os.path.join(args[0], 'build', '*.jar')):
        shutil.copy(f, args[1])

    #shutil.copyfile(os.path.join(args[0], "conf", "log4j.properties"), os.path.join(args[1], "log4j.properties"))
    
    #server = InstallServerComponents()
