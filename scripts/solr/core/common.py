import sys
import subprocess
import os
from cloud.zookeeper import ZooKeeper
from cloud.solr import Solr
from install.components import ServerComponents

class CommonCore:

    serverComponents = ServerComponents()
    zookeeper = ZooKeeper()

    def __init__(self):
        self.CheckRootUser()

    def CheckRootUser(self):
        if not os.geteuid() == 0:
            sys.exit("This script must be run as root or by using the 'sudo' command")

    def InstallPrereqs(self): 
        self.serverComponents.InstallPrereqs()

    def InstallCloud(self):
        solrInstance = Solr()
        zkEnsemble = ZooKeeper()
        pass

    def InstallTomcat(self):
        self.serverComponents.Install()

    def InstallZooKeeper(self):
        self.zookeeper.Install()
