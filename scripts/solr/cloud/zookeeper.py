import os
import subprocess
import shutil

class ZooKeeper:    

    HOME_PATH = ""
    VERSION = "3.4.5"
    FOLDER = "zookeeper-" + VERSION
    ZK_DOWNLOAD = FOLDER + ".tar.gz"
    INSTALL_FOLDER = "/usr/local/"
    BASE_FOLDER = INSTALL_FOLDER + "current"
    DATA_DIR = "/usr/local/current/var/data"
    DATA_LOG = "/usr/local/current/var/datalog"
    DOWNLOAD_URL = 'http://www.poolsaboveground.com/apache/zookeeper/' + FOLDER + '/' + ZK_DOWNLOAD

    def __init__(self):
        pass
 
    def Install(self):
        if not os.getenv("SUDO_USER") is None:
            self.HOME_PATH = "/home" + os.getenv("SUDO_USER")

        strOldDataDir = "dataDir=/tmp/zookeeper"
        strNewDataDir = "dataDir=%s" % self.DATA_DIR
 
        os.chdir("/tmp")
                
        if not os.path.exists(self.ZK_DOWNLOAD):
            subprocess.call(['wget',self.DOWNLOAD_URL])

        if(os.path.exists(self.INSTALL_FOLDER + self.FOLDER)):
            print "%s is installed" % self.FOLDER
            return         

        if(os.path.isfile(self.ZK_DOWNLOAD)):
            subprocess.call(['tar','xvfz', self.ZK_DOWNLOAD])
            subprocess.call(['cp','-R', self.FOLDER, self.INSTALL_FOLDER])
            os.chdir(self.INSTALL_FOLDER)
            if(not os.path.islink('current')):
                subprocess.call(['ln', '-s', self.FOLDER, 'current'])
            shutil.rmtree("/tmp/" + self.FOLDER)

        if not os.path.exists(self.DATA_DIR):
            subprocess.call(['mkdir','-p', self.DATA_DIR])

        if not os.path.exists(self.DATA_LOG):
            subprocess.call(['mkdir','-p', self.DATA_LOG])

        if not os.path.exists(self.BASE_FOLDER + '/conf/zoo.cfg'):
            subprocess.call(['cp',self.BASE_FOLDER + '/conf/zoo_sample.cfg',self.BASE_FOLDER + '/conf/zoo.cfg'])
            
            print "Creating ZooKeeper configuration file.."
            os.chdir(self.INSTALL_FOLDER + 'current/conf')
            with open('zoo.cfg') as zf:
                file_lines = zf.readlines()
                new_file = [line.replace(strOldDataDir,strNewDataDir) for line in file_lines]
            open('zoo.cfg', 'w').write(''.join(new_file))

            FILE = "java.env"
            fo = open(FILE, "w+")
            fo.write("JAVA_HOME=/usr/java/latest\n")
            fo.write("export PATH=$JAVA_HOME/bin:$PATH")
            fo.close()

        print "Finished creating ZooKeeper configuration file at '%s' for further manual configuration" %self.BASE_FOLDER
        os.chdir(self.BASE_FOLDER + "/bin")
        subprocess.call(['sh','zkServer.sh','stop'], shell=True)
        subprocess.call(['sh','zkServer.sh','start'], shell=True)
