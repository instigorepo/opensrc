import os
import subprocess
import shutil
import sys
from random import randint
from time import sleep

class Solr:
    
    tomcatVersion = "tomcat-8"
    tomcatBuild = "8.5.4"
    tomcatDirectory = "apache-tomcat-" + tomcatBuild
    HOME_PATH = "/opt/"
    TMP_PATH = "/tmp"
    CATALINA_HOME = HOME_PATH + tomcatDirectory
    FILE_EXTENSION = ".tgz"
    SOLR_PATH = "/opt/solr/"
    SOLR_VERSION = "4.9.1"
    SOLR_FILENAME = "solr-" + SOLR_VERSION + FILE_EXTENSION
    SOLR_BASE = "solr-" + SOLR_VERSION
    SOLR_XML = "solr.xml"
    TOMCAT_RESTART = "/etc/init.d/tomcat restart"
    ServerList = []

    solrAdminPath = "/admin/cores"
    solrDefaultCoreName = "collection1"
    solrDefaultInstanceDir = "collection1"
    solrDefaultHostPort = "8080"

    def __init__(self):
        self.Install() 
    
    def GetSolrServerName(self):
        self.ServerList.append("http://archive.apache.org/dist/lucene/solr/%s" % self.SOLR_VERSION)
        return self.ServerList[0]
    
    def CreateSolrXML(self):

        if (os.path.exists(self.SOLR_XML)):
            subprocess.call(["cp", self.SOLR_XML, self.SOLR_XML + ".old"])

        if (not os.path.exists(self.SOLR_PATH)):
            os.makedirs(self.SOLR_PATH)

        fo = open(self.SOLR_PATH + "/solr.xml", "w")
        fo.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
        fo.write("<solr persistent=\"true\" sharedLib=\"lib\">\n")
        fo.write("\t<cores adminPath=\"%s\" zkClientTimeout=\"${zkClientTimeout:15000}\" hostPort=\"%s\" hostContext=\"${hostContext:}\" >\n" % (self.solrAdminPath, self.solrDefaultHostPort))
        fo.write("\t</cores>\n")
        fo.write("</solr>\n")
        fo.close()

    def Install(self):
        self.CreateSolrXML() 
        ServerName = self.GetSolrServerName()
        
        # store the downloaded file in the tmp folder
        os.chdir(self.TMP_PATH)        
        
	#if( os.path.exists(self.SOLR_PATH)):
        #    print "Solr is installed..."
        #    return
        #else:
        print "Preparing to install Solr..."
  
        try:
            subprocess.call(['sed','-i','114i export JAVA_OPTS=\"$JAVA_OPTS -Dsolr.solr.home=/opt/solr\"\n', self.CATALINA_HOME + "/bin/catalina.sh"])
            subprocess.call(['sed','-i','115i export CATALINA_OPTS=\"$CATALINA_OPTS -Xms128m -Xmx1024m -XX:MaxPermSize=256m\"\n', self.CATALINA_HOME + "/bin/catalina.sh"])
        except subprocess.CalledProcessError:
            print 'error occurred'       
        # download SOLR from the apache web server
        if( not os.path.exists(self.SOLR_FILENAME)):
            subprocess.call(['wget', ServerName + "/" + self.SOLR_FILENAME])        
        subprocess.call(['tar','xvfz', self.SOLR_FILENAME])

        # Copy Solr configuration from the example Solr setup
        subprocess.call(['cp', '-R', '%s/%s/example/solr' % (self.TMP_PATH,self.SOLR_BASE), '%s' % self.HOME_PATH])
        subprocess.call(['cp', '-R', '%s/%s/example/solr' % (self.TMP_PATH,self.SOLR_BASE), '%s' % self.CATALINA_HOME + "/webapps"])
 
        # Copy .war file to Tomcat webapps directory
        SolrWarSrc = "%s/%s/dist/%s.war" % (self.TMP_PATH,self.SOLR_BASE,self.SOLR_BASE)
        solrDest = "%s/solr.war" % self.SOLR_PATH
        tomcatDest = "%s/webapps/solr.war" % self.CATALINA_HOME
        subprocess.call(['cp',SolrWarSrc,solrDest])
        subprocess.call(['cp',SolrWarSrc,tomcatDest])
                
        # Tell Tomcat about Solr webapp
        CatalinaLocalhost = self.CATALINA_HOME + "/conf/Catalina/localhost"
        CatalinaConfigFolder = self.CATALINA_HOME + "/conf/"

        if not os.path.isdir(CatalinaLocalhost):
            subprocess.call(['mkdir', '-p', CatalinaLocalhost])
         
        if (not os.path.exists(self.SOLR_PATH + "/data")):
            subprocess.call(['mkdir', '-p', self.SOLR_PATH + "/data"])

        os.chdir(CatalinaLocalhost)

        subprocess.call(['touch', '%s' % self.SOLR_XML])
        
        # create solr.xml file under '<apache_path>/conf/Catalina/localhost' path
        fo = open(self.SOLR_XML, "w+")
        fo.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
        fo.write("<Context allowLinking=\"true\" crossContext=\"true\" debug=\"0\" docBase=\"%s/solr.war\" privileged=\"true\">\n" % self.SOLR_PATH)
        fo.write("    <Environment name=\"solr/home\" override=\"true\" type=\"java.lang.String\" value=\"%s\"/>\n" % self.SOLR_PATH)
        fo.write("</Context>\n")
        fo.close()
       
        print "Attempting to restart Tomcat..."
        shutdownCmd = "%s/bin/shutdown.sh" % self.CATALINA_HOME
        startupCmd = "%s/bin/startup.sh" % self.CATALINA_HOME
        subprocess.call(shutdownCmd, shell=True)
        subprocess.call(startupCmd, shell=True)
        print "Tomcat has restarted..."
