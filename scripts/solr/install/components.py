from decimal import Decimal
import subprocess
import re
import os
import sys
import yum
import traceback
import shutil
import psutil
import pwd
import grp
from time import sleep

class ServerComponents:

    hostDomain = "archive.apache.org"
    tomcatVersion = "tomcat-8"
    tomcatBuild = "8.5.4"
    fileExtension = ".tar.gz"
    tomcatFilename = "apache-tomcat-" + tomcatBuild + fileExtension
    tomcatDirectory = "apache-tomcat-" + tomcatBuild
    tomcatURL = "http://" + hostDomain + "/dist/tomcat/" + tomcatVersion + "/v" + tomcatBuild + "/bin/" + tomcatFilename
    tomcatPort = "8080"
    tomcatConnectionTimeout = "20000"
    tomcatRedirectPort = "8443"
    tomcatBase = "/opt/"
    catalinaHome = tomcatBase + tomcatDirectory
    CatalinaConfigFolder = catalinaHome + "/conf/" 

    javaVersion = "jdk1.7.0_06"
    javaLib = "/usr/java/latest"
    jdkFilename = ""
    jdkFilename_64 = "jdk-8u60-linux-x64.rpm"
    jdkFilename_32 = "jdk-8u60-linux-i586.rpm"
    localJavaShare = "/usr/local/java/"
    javaBinaryPath = "/usr/bin/java"
    tmpFolder = "/tmp/"
    oracleUrl = ""
    javaHome = javaLib 
    isJavaInstalled = False
    is64bits = sys.maxsize > 2**32
    RPM_FIND = "rpm -qa | grep"
    YUM_FIND = "yum list | grep"
    YUM_GROUP_FIND = "yum grouplist | grep"
    yb = yum.YumBase()

    def __init__(self):
        if self.is64bits:
            self.jdkFilename = self.jdkFilename_64
            self.oracleUrl = "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/%s" % self.jdkFilename_64
        else:
            self.jdkFilename = self.jdkFilename_32
            self.oracleUrl = "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/%s" % self.jdkFilename_32

    def InstallPrereqs(self):
        self.InstallNTP()
        #self.InstallJava()
        #self.InitializeEnvironment()

    def Install(self):
        self.InstallTomcat()
 
    def SearchFile(self, pattern, filename):

        with open(filename) as f:
            f.seek(0)
            for line in f:
                m = re.search(pattern, line)
                if m:
                    return True
        return False

    # Name: GetVersion
    #
    # Description: Retrieves the current version number for Java (if installed)
    #
    def GetVersion(self):
        proc = subprocess.Popen(['java','-version'],stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        out = proc.stdout.read()
        items = out.split()

        matchobj = re.match( r'\"(\d+\.\d+)(\.\w+)\"',items[2],re.M|re.I)
        if matchobj:
            return matchobj.group(1)

    # Name: FindJavaHome
    #
    # Description: Returns the path to the JAVA_HOME environment variable
    #
    def FindJavaHome(self):
        if os.environ.has_key('JAVA_HOME'):
            return os.environ['JAVA_HOME']

    # Name: IsInstalled
    #
    # Description: Returns whether a given application is installed within RedHat
    #   
    def IsInstalled(self, strAppToFind):
        return self.yb.rpmdb.searchNevra(name='%s' %strAppToFind)

    def IsProcessRunning(self, name):
        for pid in psutil.pids():
            p = psutil.Process(pid)
            if p.name() == name:
                return True
        return False

    def UserGroupExists(self, groupName):
        try:
            grp.getgrnam(groupName)
        except KeyError:
            return False
        return True

    def UserExists(self, username):
        try:
            pwd.getpwnam(username)
        except KeyError:
            return False
        return True

    def InstallNTP(self):

        NTP_SERVER = "pool.ntp.org"
        NTP_PACKAGE = "ntp"
                
        if( not self.IsInstalled(NTP_PACKAGE)):
            print "Preparing to install NTP Daemon.."
            subprocess.call(["yum", "-y", "install", "ntp"])

        if( not self.IsProcessRunning('ntpd')):
            subprocess.call(["chkconfig","ntpd","on"])
            subprocess.Popen("systemctl start ntpd", shell=True, stdout=subprocess.PIPE) 
            subprocess.Popen("systemctl enable ntpd", shell=True, stdout=subprocess.PIPE) 
        else:
            subprocess.call(["ntpdate",NTP_SERVER])

    # Name: InstallJava
    #
    # Description: Performs the installation of Java 1.7
    #
    def InstallJava(self):
        DevToolsInstalled = False
        JavaBinaryPath = self.tmpFolder + self.jdkFilename
        
        print "Preparing to install Java..."
        subprocess.call(["yum", "-y", "update"])
        
        if( not self.IsInstalled('glibc.i686')):
            subprocess.call(["yum", "-y", "install", "glibc.i686"])
        
        if( not self.IsInstalled('wget')):
            subprocess.call(["yum", "-y", "install", "wget"])

        if ( not self.IsInstalled('Development tools') ):
            subprocess.call(["yum", "-y", "groupinstall", "Development Tools"])

        subprocess.call(["mkdir", "-p", self.localJavaShare])
        os.chdir("/tmp")
       
        self.isJavaInstalled = self.IsInstalled('jdk')
                
        if( not self.isJavaInstalled ):
            if not os.path.exists(JavaBinaryPath):
                subprocess.call(['wget','%s' % self.jdkFilename,'--no-cookies','--no-check-certificate','--header','Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie',self.oracleUrl])
                subprocess.call(['rpm','-Uvh','%s' % JavaBinaryPath])
            else:
                print "Preparing to install: %s" % JavaBinaryPath
                subprocess.call(['rpm','-Uvh','%s' % JavaBinaryPath])
                subprocess.call(['rm', '%s' % JavaBinaryPath])
           
            print 'Initialize Environment...' 
            self.InitializeEnvironment()
            subprocess.call(['alternatives','--install','/usr/bin/java','java','/usr/java/latest/jre/bin/java','20000'])
            subprocess.call(['alternatives','--install','/usr/bin/javac','javac','/usr/java/latest/bin/javac','20000'])
            subprocess.call(['alternatives','--install','/usr/bin/jar','jar','/usr/java/latest/bin/jar','20000'])
            subprocess.call(['alternatives','--set', 'java', '/usr/java/latest/jre/bin/java'])
            subprocess.call(['alternatives','--set', 'jar', '/usr/java/latest/bin/jar'])
            subprocess.call(['alternatives','--set', 'javac', '/usr/java/latest/bin/javac'])
            
   
    def InitializeEnvironment(self):
        HOME_DIR = "/home/" + os.environ["SUDO_USER"] + "/"
        os.chdir(HOME_DIR)

        # add environment variables
        # detect if this exists in the file already and return if true
        #
        FILE = ".bashrc"

        if self.SearchFile(str(self.javaHome), HOME_DIR + FILE):
            return
        
        print "Initializing Java Environment..."
        fo = open(FILE, "a")
        fo.write("export JAVA_HOME=%s\n" % self.javaHome)
        fo.write("export PATH=$PATH:%s/bin\n" % self.javaHome)
        fo.close()
 
        command = ['bash','-c', 'source ~/.bashrc']
        subprocess.Popen(command, stdout = subprocess.PIPE)
    
        
    def InstallTomcat(self):
        print "Preparing to install Tomcat Server.."
        subprocess.call(['sh','-c','`echo export JAVA_HOME=/usr/java/latest/ > /etc/profile.d/java.sh`'])
        subprocess.Popen("source /etc/profile.d/java.sh",shell = True,executable ="/bin/bash")
  
        try:
            
            if( os.path.exists(self.catalinaHome)):
                print "Tomcat is present..skipping installation"
            #    return
            
            #os.chdir(self.tmpFolder) 
            #if( not os.path.exists(self.tmpFolder + self.tomcatFilename)):
            #    print ("Downloading Tomcat Server from (%s)", self.tomcatURL)
            #    subprocess.call(['wget', self.tomcatURL])
            
            #print "Starting Tomcat File Extraction of file %s" % self.tomcatFilename
            #subprocess.call(['tar','xvzf', self.tmpFolder + self.tomcatFilename])          
            
            #if( not os.path.isdir(self.catalinaHome)):
            #    subprocess.call(['mv', self.tmpFolder + self.tomcatDirectory, self.tomcatBase])
            #else:
            #    subprocess.call(['cp', '-R', self.tmpFolder + self.tomcatDirectory, self.tomcatBase])
            
            if( not self.UserGroupExists('tomcat') ):            
                subprocess.call(['groupadd','tomcat'])
            
            if( not self.UserExists('tomcat') ):
                subprocess.call(['useradd','-s','/sbin/nologin','-g','tomcat','tomcat'])

            subprocess.call(['chown','-Rf','tomcat.tomcat', self.catalinaHome])
            subprocess.call(['chmod','755','%s' % self.tomcatBase + self.tomcatDirectory])
            subprocess.Popen("chmod +r " + self.tomcatBase + self.tomcatDirectory + "/conf/*", shell=True)
            self.CreateTomcatStartupScript()
            subprocess.call(['sh','-c','export JAVA_HOME=/usr/java/latest/bin/']) 
            subprocess.call(['sh','-c','`echo export CATALINA_HOME=/opt/%s > /etc/profile.d/tomcat.sh`' % self.tomcatDirectory])
        except SystemExit:
            pass  
        except Exception,err:
            sys.stderr.write('ERROR: %s\n' % str(err))
            traceback.print_exc()

    def CreateTomcatStartupScript(self):

        FILE = "tomcat"
        
        print "Generating Tomcat Startup Script..."
        os.chdir("/etc/init.d")
        fo = open(FILE, "w+")
        fo.write("#!/bin/sh\n")
        fo.write("#Start/stop the Tomcat service\n")
        fo.write("#\n")
        fo.write("### BEGIN INIT INFO\n")
        fo.write("# Provides:		tomcat\n")
        fo.write("# Required-Start:\n")
        fo.write("# Required-Stop:\n")
        fo.write("# Default-Start: 2 3 4\n")
        fo.write("# Default-Stop:\n")
        fo.write("# Short-Description: tomcat\n")
        fo.write("# Description: tomcat\n")
        fo.write("#\n")
        fo.write("### END INIT INFO\n")
        fo.write("\n")
        fo.write("export JAVA_HOME=%s\n" % self.javaHome)
        fo.write("export PATH=$PATH:%s\n" % self.javaHome)
        fo.write("CATALINA_HOME=/opt/%s\n" % self.tomcatDirectory)
        fo.write("\n")
        fo.write("case $1 in\n")
        fo.write("    start)\n")
        fo.write("        /bin/su tomcat %s/bin/startup.sh\n" % self.catalinaHome)
        fo.write("        ;;\n")
        fo.write("    stop)\n")
        fo.write("        /bin/su tomcat %s/bin/shutdown.sh\n" % self.catalinaHome)
        fo.write("        ;;\n")
        fo.write("    restart)\n")
        fo.write("        /bin/su tomcat %s/bin/shutdown.sh\n" % self.catalinaHome)
        fo.write("        /bin/su tomcat %s/bin/startup.sh\n" % self.catalinaHome)
        fo.write("        ;;\n")
        fo.write("esac\n")
        fo.write("exit 0\n")
        fo.close()

        subprocess.call(['chmod', '755', 'tomcat'])
        subprocess.call(['chkconfig','--add', 'tomcat'])
        subprocess.call(['chkconfig','--level', '234', 'tomcat', 'on'])

