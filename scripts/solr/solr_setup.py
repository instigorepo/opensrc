import sys
import os
from optparse import OptionParser
from core.common import CommonCore


def main():

    try:
         
        core = CommonCore()
        core.CheckRootUser()

        usage = "usage: %prog [options] arg"
        parser = OptionParser(usage)
        parser.add_option('-s', dest="install_solr", action="store_true", help='This flag when enabled will install Apache Solr', default=False)
        parser.add_option('-t', dest='install_tomcat', action="store_true", help='This flag when enabled will install Tomcat Server', default=False)
        parser.add_option('-z', dest='install_zookeeper', action="store_true", help='This flag when enabled will instal ZooKeeper',default=False)
        
        if len(sys.argv) == 1:
            parser.print_help()
            sys.exit(1)

        (options, args) = parser.parse_args()
        core.InstallPrereqs()

        if options.install_solr:
            core.InstallCloud()
        elif options.install_tomcat:
            core.InstallTomcat()
        elif options.install_zookeeper:
            zk.Install()

    except Exception, err:
        sys.stderr.write('ERROR: %s\n' % str(err))
        return 1

if __name__ == '__main__':
    sys.exit(main())

